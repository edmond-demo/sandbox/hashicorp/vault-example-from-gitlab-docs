# Vault CLI Example 2
Modified so that both attempts to retrieve from both staging and production are performed in a single job.
Running the job in a protected branch will return the production password.
Running the job in an un-protected branch will return the staging password.
